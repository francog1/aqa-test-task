# Running Cypress Tests 
This README provides instructions for running Cypress tests both locally and within a GitLab CI environment .

## Prerequisites
Before running tests, make sure you have the following installed locally:

- Node.js (Preferably the latest LTS version)
- npm (Comes with Node.js)
- Cypress


## Local Setup and Test Execution
### 1. Installing Dependencies
Navigate to your project directory and install the necessary dependencies:
```bash
npm install
```
### 2. Running Cypress Tests Locally
#### Interactive Mode:
For running Cypress tests in interactive mode with a graphical interface, use the following command:
```bash
npx cypress open
```
Select the test you wish to run from the Cypress Test Runner GUI.

#### Headless Mode:
To run all the Cypress tests in headless mode execute:
```bash
npx cypress run
```
## GitLab CI Setup and Test Execution

#### Running Tests in GitLab CI
GitLab CI/CD will automatically pick up the configuration and run the defined pipeline on subsequent pushes.

#### Commit and Push Changes: 
Ensure all changes, including the Cypress tests and .gitlab-ci.yml, are committed and pushed to the GitLab repository.
```bash
git add .
git commit -m "Add Cypress tests and CI/CD configuration"
git push
```
#### Viewing Pipeline Results: 
Navigate to the project on GitLab, then go to CI/CD > Pipelines. Click on a pipeline to view its runs and status.

#### Test Artifacts: 
HTML report will be generated and can be downloaded from the job page within a pipeline run.