import page from '../fixtures/page.json'

Cypress.Commands.add('fetchPublicAPIs', () => {
    cy.request({
        url: page.apiURL,
        method: page.method,
    })
        .then((response) => {
            cy.wrap(response.status).should('eq', 200);
            const apiCategory = response.body.entries.filter(entry => entry.Category === 'Authentication & Authorization')
            cy.wrap(apiCategory).should('have.length', 7)
            cy.wrap(apiCategory)

            cy.log(`There are ${apiCategory.length} elements with the "Authentication & Authorization" category:`);
            cy.log(apiCategory);

            cy.log(`There are ${apiCategory.length} elements with the "Authentication & Authorization" category:`);
            for (let i = 0; i < apiCategory.length; i++) {
                cy.log('----------------')
                cy.log('Element ' + [i + 1])
                cy.log('API: ' + apiCategory[i].API)
                cy.log('Description: ' + apiCategory[i].Description)
                cy.log('Auth: ' + apiCategory[i].Auth)
                cy.log('HTTPS: ' + apiCategory[i].HTTPS)
                cy.log('Cors: ' + apiCategory[i].Cors)
                cy.log('Link: ' + apiCategory[i].Link)
                cy.log('Category: ' + apiCategory[i].Category)
            }
        })


});

Cypress.Commands.add('login', () => {
    cy.clearLocalStorage()
    cy.get(page.selectors.usernameInput).type(page.userName)
    cy.get(page.selectors.passwordInput).type(page.password)
    cy.get(page.selectors.loginButton).click()
    cy.url().should('eq', page.uiUrl + 'inventory.html')

})

Cypress.Commands.add('validateSorting', () => {
    cy.get(page.selectors.productSort).should('contain', 'Name (A to Z)')

    cy.get(page.selectors.inventoryName).then($names => {
        const names = $names.toArray().map(el => el.innerText);
        const sorted = [...names].sort();
        expect(names[0]).to.eq(sorted[0]);
        expect(names[names.length - 1]).to.eq(sorted[sorted.length - 1]);
    });
    cy.get(page.selectors.productSort).select('Name (Z to A)')

    cy.get(page.selectors.inventoryName).then($names => {
        const names = $names.toArray().map(el => el.innerText);
        const sorted = [...names].sort().reverse();
        expect(names[0]).to.eq(sorted[0]);
        expect(names[names.length - 1]).to.eq(sorted[sorted.length - 1]);
    });

})

