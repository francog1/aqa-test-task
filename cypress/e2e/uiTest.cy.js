
import page from '../fixtures/page.json'


describe('UI Test', () => {

    beforeEach('Should do a valid login ', () => {
        cy.visit(page.uiUrl)

        cy.login()
    })
    it('Should validate sorting', () => {
        cy.validateSorting();
    });
});

